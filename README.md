项目介绍
react简单demo：经典案例TODOList和摘苹果案例 使用storybook进行组件测试 采用react react-router redux等基本react技术栈，能完成中小型项目架构搭建

软件架构
react / redux / react-router-dom / axios / less

安装教程
'npm install'

使用说明
1. 'npm run start'
可以在 http://localhost:3000/ 看到项目已启动
2. 'npm run storybook'
可以在 http://localhost:9009/ 看到组件测试环境
3. 'npm run build'
编译打包代码

