import React from 'react';
import './index.less';
import '../../styles/common.less'
import AppleItem from '../AppleItem';


class AppleBasket extends React.Component {
  render(){
    const {appleBasket,actions} = this.props;
    let stats = {
      isPicker:appleBasket.isPicker,
      eat:{num:0,weight:0},
      noeat:{num:0,weight:0},
      apples:[]
    }
    appleBasket.apples.map(elem => {
      let name = elem.isEaten?"eat":"noeat";
      stats[name].num++;
      stats[name].weight += elem.weight;
      if (!elem.isEaten) {
        stats.apples.push(elem);
      }
    })
    function getNoApple(){
      if (stats.apples.length===0 && !stats.isPicker) {
        return <div className='no-apple'>篮子空空如也，快去摘苹果吧</div>
      }
      return;
    }
    const that = this;
    function getApples(){
      let data = [];
      if (!stats.isPicker) {
        data.push(stats.apples.map((apple,index) => <AppleItem key={index} apple={apple} eatApple={that.props.actions.eatApple}/> ))
      }else{
        return <div className='no-apple'>正在采摘苹果...</div>
      }
      return data;
    }
    return(
      <div className="apple-basket">
        <div className="title">苹果篮子</div>
        <div className="stats row">
          <div className='col current'>
            <div className="label">当前</div>
            <div><span>{stats.noeat.num}</span>个苹果，<span>{stats.noeat.weight}</span>克</div>
          </div>
          <div className='col eat'>
            <div className="label">已吃掉</div>
            <div><span>{stats.eat.num}</span>个苹果，<span>{stats.eat.weight}</span>克</div>
          </div>
        </div>
        <div className="apple-list col">
          {getApples()}
          {getNoApple()}
        </div>
        <div className="btn-panel row"><button className={stats.isPicker ? 'disabled' : ''} onClick={actions.pickApple}>摘苹果</button></div>
      </div>
    )
  }
}
export default AppleBasket;
