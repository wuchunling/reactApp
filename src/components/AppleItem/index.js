import React from 'react';
import './index.less';
import '../../styles/common.less';
import appleImg from '../../assets/apple.png'
class AppleItem extends React.Component {
  render(){
    let {apple,eatApple} = this.props;
    return(
      <div className="apple-item row">
        <div className='row'>
          <div className="apple"><img src={appleImg} alt="苹果图片"/></div>
          <div className="info">
            <div className="name">红苹果 - {apple.id}号</div>
            <div className="weight">{apple.weight}克</div>
          </div>
        </div>
        <div className="btn-div"><button onClick={eatApple.bind(this,apple.id)}>吃掉</button></div>
      </div>
    )
  }
}
export default AppleItem;
