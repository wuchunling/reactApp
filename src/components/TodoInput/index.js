import React from 'react';
import './index.less'
class TodoInput extends React.Component {
  addHandler=()=>{
    let Inp = this.refs.Inp;
    if(!Inp.value){ return; }
    this.props.addItem(Inp.value);
    Inp.value = ''; //提交后，清空输入
    Inp.focus(); //重置输入焦点
  }
  handleEnter=(e)=>{
    if (e.keyCode==13) {
      this.addHandler()
    }
  }
  render() {
    const {addItem} = this.props;
    return(
      <div className="input-div">
        <input type='text' name="text" onKeyUp={this.handleEnter} ref="Inp"/>
        <button onClick={this.addHandler}>添加</button>
      </div>
    )
  }
}
export default TodoInput;
