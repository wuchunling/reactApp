import React from 'react';
import './index.less';
import '../../styles/common.less';

class TodoItem extends React.Component {
  render() {
    const {item,index,finishItem,delItem} = this.props;
    return (
      <div className="item row">
        <div className={item.finish?'finish':'unfinish'}>
          <input className='checkbox' type="checkbox" checked={item.finish} onChange={finishItem.bind(this,index)}/>
          <span  className='text'>{item.text}</span>
        </div>
        <div><a class='del' onClick={delItem.bind(this,index)}>删除</a></div>
      </div>
    )
  }
}
export default TodoItem;
