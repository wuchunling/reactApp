import React from 'react';
import './index.less'
import TodoItem from '../TodoItem'
import TodoInput from '../TodoInput'
class TodoList extends React.Component{
  render(){
    const {todoItems,todoActions} = this.props;
    return (
      <div className="todolist">
        <div className="header">待办事项</div>
        <div className="items">
          {todoItems.map((item,index)=> <TodoItem item={item} index={index} key={item.id} finishItem={todoActions.finishItem} delItem={todoActions.delItem}/>)}
        </div>
        <div className="footer">
          <TodoInput addItem={todoActions.addItem}/>
        </div>
      </div>
    )
  }
}
export default TodoList;
