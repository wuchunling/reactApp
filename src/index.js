import React from 'react';
import ReactDOM from 'react-dom';
import {createStore,applyMiddleware} from 'redux';
import thunk from 'redux-thunk';

import registerServiceWorker from './registerServiceWorker';
import reducer from './redux/reducers'
import Root from './pages/Root'

let store = createStore(reducer,applyMiddleware(thunk))

ReactDOM.render(
    <Root store={store}/>,
    document.getElementById('root')
);
registerServiceWorker();
