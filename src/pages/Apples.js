import React from 'react'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import AppleBasket from '../components/AppleBasket/index';
import actions from '../redux/actions/appleAction';

class Apples extends React.Component {
  constructor(props){  //初始化数据
    super(props);
    this.props.dispatch(this.props.actions.initApplesStart)
  }
  render(){
    let {appleBasket,actions,dispatch} = this.props;
    return(
      <div className="apples">
        <AppleBasket appleBasket={appleBasket} actions={actions}/>
      </div>
    )
  }
}

const mapStateToProps = state => ({
    appleBasket: state.appleBasket
});
const mapDispatchToProps = dispatch => ({
    dispatch: dispatch,
    actions: bindActionCreators(actions, dispatch)
});
export default connect(mapStateToProps, mapDispatchToProps)(Apples);
