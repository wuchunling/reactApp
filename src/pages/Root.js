import React from 'react';
import { BrowserRouter, Route, NavLink,Switch } from 'react-router-dom'
import {Provider} from 'react-redux';
import {rootRouters} from '../router'
import '../styles/common.less'
export default class Root extends React.Component{
  render(){
    const { store } = this.props;
    return(
      <Provider store={store}>
        <BrowserRouter>
          <div>
            <nav className='root-nav'>
              <NavLink exact activeClassName="active-nav" to="/apple">苹果篮子-示例</NavLink>
              <NavLink activeClassName="active-nav" to="/todo">TodoList-示例</NavLink>
            </nav>
            <Switch>
              {
                rootRouters.map((route,index)=>{
                  return (
                    <Route
                      key={index}
                      path={route.path}
                      component={route.component}
                      exact={route.exact}/>
                  )
                })
              }
            </Switch>
          </div>
        </BrowserRouter>
      </Provider>
    )
  }
}
