import React from 'react'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import TodoList from '../components/TodoList'
import todoActions from '../redux/actions/todoActions'
class Todos extends React.Component{
  render(){
    let {todoItems,todoActions} = this.props;
    return (
      <TodoList todoItems={todoItems.items} todoActions={todoActions}/>
    )
  }
}
const mapStateToProps = state => ({
  todoItems:state.todoItems
});
const mapDispatchToProps = dispatch => ({
  todoActions:bindActionCreators(todoActions,dispatch)
});
export default connect(mapStateToProps,mapDispatchToProps)(Todos);
