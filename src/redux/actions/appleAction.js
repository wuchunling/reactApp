import Apples from '../services';

let actions = {
  initApplesStart:function(){
    return function(dispatch,getState){
      Apples.init().then(function(res){
        dispatch(actions.initApplesSuccess(res));
      }, function(err){
        dispatch(actions.initApplesFail(err));
      });
    }
  },
  initApplesSuccess:(data)=>({
    type:'apple/INIT_APPLE_SUCCESS',
    payload:data
  }),
  initApplesFail:(data)=>({
    type:'apple/INIT_APPLE_FAIL',
    payload:data
  }),
  eatApple:(id) => ({
    type:'apple/EAT_APPLE',
    payload:id
  }),
  pickApple:function(){
    return function(dispatch,getState){
      if(getState().appleBasket.isPicker){
        return;
      }
      dispatch(actions.beginPickApple());
      Apples.pick().then(function(res){
        dispatch(actions.donePickApple(res.apples));
      }, function(err){
        dispatch(actions.donePickApple(err));
      });
    }
  },
  beginPickApple:() => ({
    type:'apple/BEGIN_PICK_APPLE'
  }),
  donePickApple:appleWeight => ({
    type:'apple/DONE_PICK_APPLE',
    payload:appleWeight
  }),
  failPickApple:err => ({
    type:'apple/FAIL_PICK_APPLE',
    payload:err
  })
}
export default actions;
