let todoActions = {
  delItem:id =>({
    type:'todoList/DEL_ITEM',
    payload:id
  }),
  addItem:text =>({
    type:'todoList/ADD_ITEM',
    payload:text
  }),
  finishItem:id =>({
    type:'todoList/FINISH_ITEM',
    payload:id
  })
};
export default todoActions;
