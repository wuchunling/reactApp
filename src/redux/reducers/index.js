import { combineReducers } from 'redux';
import appleReducer from './appleReducer';
import todoReducers from './todoReducers';
const rootReducer = combineReducers({
    appleBasket: appleReducer,
    todoItems:todoReducers
});

export default rootReducer;
