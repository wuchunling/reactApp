const initialState = {
  newItemId:7,
  items:[
    {id:1,text:"4月15毕业旅游",finish:true},
    {id:2,text:"吃饭",finish:true},
    {id:3,text:"睡觉",finish:false},
    {id:4,text:"打豆豆",finish:false},
    {id:5,text:"敲代码",finish:true},
    {id:6,text:"学习",finish:false}
  ]
};
const todoReducers = (state=initialState,action)=>{
  switch (action.type) {
    case 'todoList/DEL_ITEM':
      state.items.splice(action.payload,1);
      return {...state};
    case 'todoList/ADD_ITEM':
      let item = {id:state.newItemId,text:action.payload,finish:false};
      state.items.push(item);
      state.newItemId++;
      return {...state};
    case 'todoList/FINISH_ITEM':
      state.items[action.payload].finish = !state.items[action.payload].finish;
      return {...state};
    default:
      return state;
  }
}
export default todoReducers;
