import Request from '../../utils/Request';
const Apples = {
  init:() => { return Request.get('/apples',null); },
  pick:() => { return Request.get('/apples/pick',null);},
};
export default Apples;
