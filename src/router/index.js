import React from 'react'
import Loadable from 'react-loadable'
import {Redirect} from 'react-router-dom'
const loadable = (filename) => Loadable({  //路由组件懒加载
    loader:() => import(`../pages/${filename}`),
    loading:() => ('')
});

const rootRouters = [  //根路由集合
  {
    path:'/',   //根路由重定向
    exact:true,
    component:() => <Redirect to='/apple'/>
  },
  {
    path:'/apple',
    component:loadable('Apples')
  },
  {
    path:'/todo',
    component:loadable('Todos')
  },
  {
    path:'*', // 404 匹配
    component:loadable('404')
  }
];
const routes = [  //嵌套路由集合
  {
    path:'/apple',
    component:loadable('Apples')
  },
  {
    path:'/todo',
    component:loadable('Todos')
  }
]
export {
  rootRouters,
  routes
}
