import axios from 'axios';
//全局默认axios配置
axios.defaults.baseURL = 'https://easy-mock.com/mock/5b4bf8299972135bfccea463/example';
if(localStorage.token){axios.defaults.headers.common['Authorization'] = localStorage.token;}
axios.defaults.headers.post['Content-Type'] = 'application/json';


const Request = {
  get:(url,data)=>{
    return new Promise((resolve,reject)=>{
      axios.get(url, {
        params:data
      })
      .then(function (res) {
        if(res.data.code===200){resolve(res.data.data);}
        else{reject(res.data.message);}
      })
      .catch(function (err) {
        reject(err);
      })
    }).catch(function(err){
      alert(err);
    })
  },
  post:(url,data)=>{
    return new Promise((resolve,reject)=>{
      axios.post(url, data)
      .then(function (res) {
        if(res.data.code===200){
          if(url==="/login"){
            localStorage.token = res.data.data.token;
            axios.defaults.headers.common['Authorization'] = localStorage.token;
          }
          resolve(res.data.data);
        }
        else{reject(res.data.message);}
      })
      .catch(function (err) {
        reject(err);
      })
    }).catch(function(err){
      alert(err);
    })
  },
  put:(url,data)=>{
    return new Promise((resolve,reject)=>{
      axios.put(url,data)
      .then(function (res) {
        if(res.data.code===200){resolve(res.data.data);}
        else{reject(res.data.message);}
      })
      .catch(function (err) {
        reject(err);
      })
    }).catch(function(err){
      alert(err);
    })
  },
  delete:(url,data)=>{
    return new Promise((resolve,reject)=>{
      axios.delete(url,data)
      .then(function (res) {
        if(res.data.code===200){resolve(res.data.data);}
        else{reject(res.data.message);}
      })
      .catch(function (err) {
        reject(err);
      })
    }).catch(function(err){
      alert(err);
    })
  }
};
export default Request;
