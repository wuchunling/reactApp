import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import AppleItem from '../src/components/AppleItem/index';
import AppleBasket from '../src/components/AppleBasket/index';

import TodoItem from '../src/components/TodoItem/index';
import TodoInput from '../src/components/TodoInput/index';
import TodoList from '../src/components/TodoList';


const basket = {
    isPicker:false,
    newAppleId:6,
    apples:[
      {
        id:1,
        weight:230,
        isEaten:false
      },
      {
        id:2,
        weight:120,
        isEaten:true
      },
      {
        id:3,
        weight:290,
        isEaten:false
      },
      {
        id:4,
        weight:118,
        isEaten:false
      },
      {
        id:5,
        weight:280,
        isEaten:true
      }
    ]
  }
const apple = {id:3,weight:280,isEaten:false};
const appleActions = {
  eatApple: (id) => action("eatApple")(id),
  pickApple: () => action("pickApple")('摘苹果')
};
storiesOf('Apple',module)
  .add("AppleItem",()=>(<AppleItem apple={apple} eatApple={action("eatApple")}/>))
  .add("AppleBasket",()=>(<AppleBasket appleBasket={basket} actions={appleActions}/>))

const item1 = {id:1,text:"4月15毕业旅游",finish:true};
const item2 = {id:2,text:"11月11淘宝剁手",finish:false};
const todoItems = [
  {id:1,text:"4月15毕业旅游",finish:true},
  {id:2,text:"吃饭",finish:true},
  {id:3,text:"睡觉",finish:false},
  {id:4,text:"打豆豆",finish:false},
  {id:5,text:"敲代码",finish:true},
  {id:6,text:"学习",finish:false}
]
const todoActions = {
  delItem:(id)=>action("delItem")(id),
  finishItem:(id)=>action("finishItem")(id),
  addItem:(text)=>action("addItem")(text),
}
storiesOf("TodoList",module)
  .add("TodoItem1",()=>(<TodoItem item={item1} finishItem={action("finish")} delItem={action("delete")}/>))
  .add("TodoItem2",()=>(<TodoItem item={item2} finishItem={action("finish")} delItem={action("delete")}/>))
  .add("TodoInput",()=>(<TodoInput addItem={action("add")} />))
  .add("TodoList",()=>(<TodoList todoItems={todoItems} todoActions={todoActions}/>))
